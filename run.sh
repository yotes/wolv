#!/bin/sh

cd "$(dirname "$0")" || exit 1

init() {
    build_env
    ensure_quicklisp
}

with_host() {
    env CPATH="" LD_LIBRARY_PATH="" PATH=/run/wrappers/bin:/home/mulch/.nix-profile/bin:/etc/profiles/per-user/mulch/bin:/nix/var/nix/profiles/default/bin:/run/current-system/sw/bin $@
}

with_env() {
    env SRC_PATH="${PWD}/" CPATH="$PWD/result/include" PATH="$PWD/result/bin" LD_LIBRARY_PATH="$PWD/result/lib" $@
}


build_env() {
    with_host nix build .
}

ensure_quicklisp() {
    if ! test -f quicklisp/setup.lisp; then
        curl https://beta.quicklisp.org/quicklisp.lisp > /tmp/quicklisp.lisp
        result/bin/ccl -n -l /tmp/quicklisp.lisp -e '(quicklisp-quickstart:install :path #P"./.ql/")' --eval '(quit)'
    fi
    rm /tmp/quicklisp.lisp
    test -f site-init.lisp || touch site-init.lisp
}

shell() {
    with_env sh -i
}

emacs() {
    with_env result/bin/emacs --no-splash $@
}

lisp() {
    with_env result/bin/ccl -n -l "${SRC_PATH}.ql/setup.lisp" -l "${SRC_PATH}site-init.lisp"
}

func="${1}"
shift
"$func" $@
