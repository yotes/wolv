(asdf:defsystem "wolv"
  :depends-on ("tooter"
               "dexador"
               "quri"
               "str"
               "alexandria"
               "cl-json"
               "cl-cron")
  :components ((:file "src/package")
               (:file "src/bot")))
