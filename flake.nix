{
  inputs = {
    emacs.url = "github:nix-community/emacs-overlay";
    emacs.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = { self, nixpkgs, emacs }:
    let
      pkgs = import nixpkgs {
        system = "x86_64-linux";
        overlays = [ emacs.overlays.default ];
      };
      emacsY = pkgs.emacsWithPackagesFromUsePackage {
        config = ./default.el;
        defaultInitFile = true;
        package = pkgs.emacsNativeComp;
        alwaysEnsure = false;
      };
    in {
      packages."x86_64-linux".default = pkgs.buildEnv {
        name = "wolv-env";
        paths = with pkgs; [ coreutils gcc curl git openssh bash ccl openssl.out sqlite.out emacsY glibc.out ];
        pathsToLink = [ "/share" "/bin" "/lib" "/include" ];
        extraOutputsToInstall = [ "lib" ];
      };
    };
}
