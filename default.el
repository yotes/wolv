(require 'use-package)

(use-package emacs
  :init
  (setq inhibit-startup-screen t
        make-backup-files nil
        backup-by-copying t
        create-lockfiles nil
        auto-save-default nil
        use-dialog-box nil
        show-paren-style 'parenthesis
        focus-follows-mouse 1
        mouse-autoselect-window t
        use-short-answers t
        enable-recursive-minibuffers t
        minibuffer-prompt-properties '(read-only t cursor-intangible t face minibuffer-prompt))
  (setq-default indent-tabs-mode nil
                tab-width 4)
  :config
  (tool-bar-mode -1)
  (scroll-bar-mode -1)
  (menu-bar-mode -1)
  (tooltip-mode -1)
  (blink-cursor-mode -1)
  (delete-selection-mode 1)
  (show-paren-mode 1)
  (global-display-line-numbers-mode 1)
  (add-hook 'minibuffer-setup-hook #'cursor-intangible-mode)
  (set-face-attribute 'default nil
                      :font "PragmataPro Liga-12"))

(use-package ef-themes
  :config (ef-themes-select
           (nth (mod (cl-parse-integer (secure-hash 'sha1
                                                    (or (getenv "SRC_PATH")
                                                        ""))
                                       :radix 16)
                     (length ef-themes-dark-themes))
                ef-themes-dark-themes))
  :ensure t)

(use-package company
  :init
  (setq company-idle-delay 0
        company-mimimum-prefix-length 1
        company-selection-wrap-around t)
  :config
  (global-company-mode)
  :ensure t)

(use-package undo-tree
  :init
  (setq undo-tree-history-directory-alist
        `(("." . ,(expand-file-name "~/.emacs.d/undo-tree/"))))
  :config (global-undo-tree-mode)
  :ensure t)

;; UX packages
(use-package vertico
  :init
  :config
  (vertico-mode t)
  :ensure t)
(use-package vertico-flat
  :after vertico)
(use-package vertico-multiform
  :preface
  ;; Sort directories before files
  (defun me/vertico-sort-directories-first (files)
    (setq files (vertico-sort-history-length-alpha files))
    (nconc (seq-filter (lambda (x) (string-suffix-p "/" x)) files)
           (seq-remove (lambda (x) (string-suffix-p "/" x)) files)))
  :after (vertico vertico-flat)
  :init
  (setq vertico-multiform-commands
        '((execute-extended-command flat))
        vertico-multiform-categories
        '((buffer flat (vertico-cycle . t))
          (file (vertico-sort-function . me/vertico-sort-directories-first))))
  :config
  (vertico-multiform-mode))
(use-package vertico-directory
  :after vertico
  :bind (:map vertico-map
              ("RET" . vertico-directory-enter)
              ("DEL" . vertico-directory-delete-char)
              ("M-DEL" . vertico-directory-delete-word))
  :hook (rfn-eshadow-update-overlay . vertico-directory-tidy))

(use-package orderless
  :init
  (setq completion-styles '(orderless basic)
        completion-category-defaults nil
        completion-category-overrides
        '((file (styles partial-completion))))
  :ensure t)
(use-package ctrlf
  :config (ctrlf-mode t)
  :ensure t)
(use-package consult
  :ensure t)
(use-package marginalia
  :config (marginalia-mode t)
  :ensure t)
(use-package embark
  :ensure t
  :init
  (setq prefix-help-command #'embark-prefix-help-command)
  :config
  (global-set-key (kbd "C-.") #'embark-act)
  (global-set-key (kbd "C-;") #'embark-dwim)
  (global-set-key (kbd "C-h B") #'embark-bindings)
  
  (add-to-list 'display-buffer-alist
               '("\\`\\*Embark Collect \\(Live\\|Completions\\)\\*"
                 nil
                 (window-parameters (mode-line-format . none)))))
(use-package embark-consult
  :ensure t
  :hook (embark-collect-mode . consult-preview-at-point-mode))

(use-package which-key
  :config
  (which-key-setup-minibuffer)
  (which-key-mode)
  :ensure t)
(use-package savehist
  :config (savehist-mode))

(use-package magit
  :ensure t)
(use-package rainbow-delimiters
  :hook prog-mode
  :ensure t)

(use-package nix-mode
  :ensure t)

;; Project-specific packages go here ↓
(use-package sly
  :init
  (let ((src-path (getenv "SRC_PATH")))
    (setq inferior-lisp-program (expand-file-name "result/bin/ccl" src-path))
    (setq sly-lisp-implementations
          `((ccl (,(expand-file-name "result/bin/ccl" src-path)
                  "-n"
                  "-l" ,(expand-file-name ".ql/setup.lisp" src-path)
                  "-l" ,(expand-file-name "site-init.lisp" src-path))))))
  :ensure t)
(use-package lispy
  :hook lisp-mode           
  :ensure t)
