(in-package net.wrwlf.wolv)
(defvar *mastodon-instance* "https://critter.zone")
(defvar *client* (make-instance 'tooter:client
                                :base *mastodon-instance*
                                :name "wolv"))
(defvar *keyfile* #p"key.txt")

(defvar *animal* "yote")
(defvar *endpoint* "https://api.tinyfox.dev")
(defvar *cronjob* nil)

(defun get-image-url ()
  (let* ((uri (quri:uri (str:concat *endpoint* "/img?json&animal=" *animal*)))
         (response (json:decode-json-from-string (dexador:get uri))))
    (quri:uri (str:concat *endpoint* (alexandria:assoc-value response :loc)))))

(defun toot-image (uri)
  (let* ((ext (pathname-type (quri:uri-file-pathname uri)))
         (path (parse-namestring (str:concat "/tmp/image." ext))))
    (dex:fetch uri path)
    (let ((attachment (tooter:make-media *client* (parse-namestring path))))
      (tooter:make-status *client* "Today's coyote is..."
                          :media attachment
                          :sensitive T))
    (delete-file path)))

(defun authorize (&optional key)
  (if key (progn (tooter:authorize *client* key)
                 (str:to-file
                  *keyfile*
                  (prin1-to-string `((access-token . ,(tooter:access-token *client*))
                                     (key . ,(tooter:key *client*))
                                     (secret . ,(tooter:secret *client*)))))
                 T)
      (progn (multiple-value-bind (result url)
                 (tooter:authorize *client*)
               (format T "Please, go to the following URL and call `(wolv:authorize)' again
with the API key provided by Mastodon:~%~%    ~A~%" url)
               result))))

(defun read-credentials ()
  (let ((credentials (read-from-string (str:from-file *keyfile*))))
    (setf (tooter:access-token *client*) (alexandria:assoc-value credentials 'access-token)
          (tooter:key *client*) (alexandria:rassoc-value credentials 'key)
          (tooter:secret *client*) (alexandria:rassoc-value credentials 'secret))
    (tooter:verify-credentials *client*)))

(defun toot-random-image ()
  (toot-image (get-image-url)))

;; Running helpers
(defun %start-cron ()
  (cl-cron:make-cron-job #'toot-random-image :hour 0 :minute 0 :hash-key :toot)
  (cl-cron:start-cron))
(defun %stop-cron ()
  (cl-cron:delete-cron-job :toot)
  (cl-cron:stop-cron))
